package cn.jfast.expand.wx.api.core;

import java.util.Map;

import cn.jfast.expand.wx.api.msg.MPAct;

public interface MpActResource {
	
	Map<String,MPAct> getResources();
	
}
