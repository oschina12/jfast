/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base.cache;

import java.util.Map;

/**
 * 缓存接口
 * @author 泛泛o0之辈
 */
public interface Cache {

	/** 获得缓存名 */
	String getName();

	/** 获得缓存实际提供者 */
	Map<Object,Object> getNativeCache();

	/** 获得缓存对象 */
	Object get(Object key);

	boolean containsKey(Object key);

	/** 获得指定类型的缓存对象 */
	<T> T get(Object key, Class<T> type);

	/** 添加到缓存 */
	void put(Object key, Object value);

	/** 缓存大小 */
	int size();

	/** 清空缓存 */
	void clear();

}
