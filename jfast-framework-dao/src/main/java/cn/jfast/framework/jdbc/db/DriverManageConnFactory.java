/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DriverManageConnFactory {

    private Connection conn;
    private static DriverManageConnFactory me;

    public static DriverManageConnFactory me() {
        if (null == me)
            me = new DriverManageConnFactory();
        return me;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DBProp.jdbc_driver);
        conn = DriverManager.getConnection(DBProp.getJdbcUrl(), DBProp.jdbc_user, DBProp.jdbc_password);
        return conn;
    }

}
