package cn.jfast.plugin.wizards;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

public class GenCodePathPage extends WizardPage {

	private ISelection selection;
	private Text actionPath;

	private boolean[] generateRules = new boolean[] { true, true, true, true, true, true ,true};
	private Text servicePath;
	private Text daoPath;
	private Text modelPath;

	/**
	 * Create the wizard.
	 */
	public GenCodePathPage() {
		super("wizardPage");
		setTitle("路径设置");
		setDescription("设置相关文件生成以及存放路径!");
	}
	
	public GenCodePathPage(ISelection selection) {
		super("路径设置页");
		this.selection = selection;
		setTitle("路径设置");
		setDescription("设置相关文件生成以及存放路径!");
	}


	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		setControl(container);
		Group group = new Group(container, SWT.NONE);
		group.setText("生成文件");
		group.setBounds(10, 10, 564, 78);
		final Button btnCheckButton = new Button(group, SWT.CHECK);
		btnCheckButton.setSelection(true);
		btnCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[0] = btnCheckButton.getSelection();
				dialogChage();
			}
		});
		btnCheckButton.setBounds(10, 24, 98, 17);
		btnCheckButton.setText("控制类");
		final Button btnCheckButton_1 = new Button(group, SWT.CHECK);
		btnCheckButton_1.setSelection(true);
		btnCheckButton_1.setBounds(114, 24, 98, 17);
		btnCheckButton_1.setText("业务接口");
		btnCheckButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[1] = btnCheckButton_1.getSelection();
				dialogChage();
			}
		});

		final Button btnCheckButton_2 = new Button(group, SWT.CHECK);
		btnCheckButton_2.setSelection(true);
		btnCheckButton_2.setBounds(220, 24, 98, 17);
		btnCheckButton_2.setText("业务实现");
		btnCheckButton_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[2] = btnCheckButton_2.getSelection();
				dialogChage();
			}
		});

		final Button btnCheckButton_3 = new Button(group, SWT.CHECK);
		btnCheckButton_3.setSelection(true);
		btnCheckButton_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[3] = btnCheckButton_3.getSelection();
				dialogChage();
			}
		});
		btnCheckButton_3.setBounds(324, 24, 98, 17);
		btnCheckButton_3.setText("数据接口");

		final Button btnCheckButton_4 = new Button(group, SWT.CHECK);
		btnCheckButton_4.setSelection(true);
		btnCheckButton_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[4] = btnCheckButton_4.getSelection();
				dialogChage();
			}
		});
		btnCheckButton_4.setBounds(10, 47, 98, 17);
		btnCheckButton_4.setText("数据实现");
		final Button btnCheckButton_5 = new Button(group, SWT.CHECK);
		btnCheckButton_5.setSelection(true);
		btnCheckButton_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[5] = btnCheckButton_5.getSelection();
				dialogChage();
			}
		});
		btnCheckButton_5.setBounds(114, 47, 98, 17);
		btnCheckButton_5.setText("实体类");
		
		final Button btnCheckButton_6 = new Button(group, SWT.CHECK);
		btnCheckButton_6.setSelection(true);
		btnCheckButton_6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				generateRules[6] = btnCheckButton_6.getSelection();
				dialogChage();
			}
		});
		btnCheckButton_6.setBounds(220, 47, 98, 17);
		btnCheckButton_6.setText("配置文件");

		Group group_1 = new Group(container, SWT.NONE);
		group_1.setText("路径设置");
		group_1.setBounds(10, 94, 564, 218);

		Label lblNewLabel = new Label(group_1, SWT.NONE);
		lblNewLabel.setBounds(10, 25, 52, 25);
		lblNewLabel.setText("控制类：");

		actionPath = new Text(group_1, SWT.BORDER);
		actionPath.setBounds(68, 26, 115, 23);
		servicePath = new Text(group_1, SWT.BORDER);
		servicePath.setBounds(68, 56, 115, 23);
		daoPath = new Text(group_1, SWT.BORDER);
		daoPath.setBounds(68, 85, 115, 23);
		modelPath = new Text(group_1, SWT.BORDER);
		modelPath.setBounds(68, 115, 115, 23);

		Button btAction = new Button(group_1, SWT.NONE);
		btAction.setBounds(189, 24, 52, 27);
		btAction.setText("浏览...");

		Label lblNewLabel_2 = new Label(group_1, SWT.NONE);
		lblNewLabel_2.setBounds(10, 55, 55, 25);
		lblNewLabel_2.setText("业务类：");
		Button btService = new Button(group_1, SWT.NONE);
		btService.setText("浏览...");
		btService.setBounds(189, 56, 52, 27);
		Label label_2 = new Label(group_1, SWT.NONE);
		label_2.setBounds(10, 85, 55, 25);
		label_2.setText("数据类：");
		Label lblNewLabel_3 = new Label(group_1, SWT.NONE);
		lblNewLabel_3.setBounds(10, 115, 55, 25);
		lblNewLabel_3.setText("实体类：");
		Button btDao = new Button(group_1, SWT.NONE);
		btDao.setText("浏览...");
		btDao.setBounds(189, 85, 52, 27);
		Button btModel = new Button(group_1, SWT.NONE);
		btModel.setText("浏览...");
		btModel.setBounds(189, 115, 52, 27);

		btAction.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerSelectionDialog dialog = new ContainerSelectionDialog(getShell(),
						ResourcesPlugin.getWorkspace().getRoot(), false, "选择控制类存放路径 !");
				if (dialog.open() == ContainerSelectionDialog.OK) {
					Object[] result = dialog.getResult();
					if (result.length == 1) {
						getActionPath().setText(((Path) result[0]).toString());
					}
				}
			}
		});
		btService.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerSelectionDialog dialog = new ContainerSelectionDialog(getShell(),
						ResourcesPlugin.getWorkspace().getRoot(), false, "选择业务类存放 路径 !");
				if (dialog.open() == ContainerSelectionDialog.OK) {
					Object[] result = dialog.getResult();
					if (result.length == 1) {
						getServicePath().setText(((Path) result[0]).toString());
					}
				}
			}
		});
		btDao.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerSelectionDialog dialog = new ContainerSelectionDialog(getShell(),
						ResourcesPlugin.getWorkspace().getRoot(), false, "选择视数据类存放 路径 !");
				if (dialog.open() == ContainerSelectionDialog.OK) {
					Object[] result = dialog.getResult();
					if (result.length == 1) {
						getDaoPath().setText(((Path) result[0]).toString());
					}
				}
			}
		});
		btModel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerSelectionDialog dialog = new ContainerSelectionDialog(getShell(),
						ResourcesPlugin.getWorkspace().getRoot(), false, "选择视实体类存放 路径 !");
				if (dialog.open() == ContainerSelectionDialog.OK) {
					Object[] result = dialog.getResult();
					if (result.length == 1) {
						getModelPath().setText(((Path) result[0]).toString());
					}
				}
			}
		});

		actionPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				dialogChage();
			}
		});
		servicePath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				dialogChage();
			}
		});
		daoPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				dialogChage();
			}
		});
		modelPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				dialogChage();
			}
		});
		dialogChage();
	}

	private void dialogChage() {
		if (generateRules[0]) {// 选择生成时才难
			String path = actionPath.getText();
			IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(path));
			if (path.length() == 0) {
				updateStatus("设置控制类路径");
				return;
			}
			if (container == null || (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
				updateStatus(path + "不存在!");
				return;
			}
			if (!container.isAccessible()) {
				updateStatus(path + "不可写!");
				return;
			}
		}
		if (generateRules[1] || generateRules[2]) {// 选择生成时才难
			if(!generateRules[1] && generateRules[2]){
				updateStatus("选择业务实现时,业务接口必选");
				return;
			}			
			String path = servicePath.getText();
			IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(path));
			if (path.length() == 0) {
				updateStatus("设置业务类路径");
				return;
			}
			if (container == null || (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
				updateStatus(path + "不存在!");
				return;
			}
			if (!container.isAccessible()) {
				updateStatus(path + "不可写!");
				return;
			}
		}
		if (generateRules[3] || generateRules[4]) {// 选择生成时才难
			if(!generateRules[3] && generateRules[4]){
				updateStatus("选择数据实现时,数据接口必选");
				return;
			}		
			String path = daoPath.getText();
			IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(path));
			if (path.length() == 0) {
				updateStatus("设置数据类路径");
				return;
			}
			if (container == null || (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
				updateStatus(path + "不存在!");
				return;
			}
			if (!container.isAccessible()) {
				updateStatus(path + "不可写!");
				return;
			}
		}
		if (generateRules[5]) { // 选择生成时才难
			String path = modelPath.getText();
			IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(path));
			if (path.length() == 0) {
				updateStatus("设置实体类路径");
				return;
			}
			if (container == null || (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
				updateStatus(path + "不存在!");
				return;
			}
			if (!container.isAccessible()) {
				updateStatus(path + "不可写!");
				return;
			}
		}
		if (generateRules[6]) {
			if(!generateRules[0] 
					&& !generateRules[1]
					&& !generateRules[2]
					&& !generateRules[3]
					&& !generateRules[4]
					&& !generateRules[5]){
				updateStatus("不能单独生成配置文件");
				return;
			}
		}
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);

	}

	public ISelection getSelection() {
		return selection;
	}

	public void setSelection(ISelection selection) {
		this.selection = selection;
	}

	public Text getActionPath() {
		return actionPath;
	}

	public void setActionPath(Text actionPath) {
		this.actionPath = actionPath;
	}
	
	public boolean[] getGen() {
		return generateRules;
	}

	public void setGen(boolean[] gen) {
		this.generateRules = gen;
	}

	public Text getServicePath() {
		return servicePath;
	}

	public void setServicePath(Text servicePath) {
		this.servicePath = servicePath;
	}

	public Text getDaoPath() {
		return daoPath;
	}

	public void setDaoPath(Text daoPath) {
		this.daoPath = daoPath;
	}

	public Text getModelPath() {
		return modelPath;
	}

	public void setModelPath(Text modelPath) {
		this.modelPath = modelPath;
	}

}
